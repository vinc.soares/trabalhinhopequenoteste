/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hamcrest.core.Is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dvk
 */
public class JogoTest {
    
    @Test
    public void criarListaJogosSizeCheckTest() {
        Partida partida = new Partida();
        Usuario usuario = new Usuario();
        partida.setUsuario(usuario);
        Jogo jogo = new Jogo();
        jogo.setPartida(partida);
        
        assertThat(Jogo.criarJogosAleatorio(partida, 15).size(), Is.is(15));
    }
    
    @Test
    public void estaCertoTrueTest(){
        
        Jogo jogoTest = new Jogo();
        jogoTest.setResposta(5.0);
        jogoTest.setResultado(5);
        
        assertTrue(jogoTest.estaCerto());
    }
    
    @Test
    public void estaCertoFalseTest(){
        
        Jogo jogoTest = new Jogo();
        jogoTest.setResposta(5.0);
        jogoTest.setResultado(10);
        
        assertFalse(jogoTest.estaCerto());
    }
    
    @Test
    public void toStringTest(){
        
        Jogo jogoTest = new Jogo();
        jogoTest.setIdjogo(5);
        assertThat("br.edu.vianna.plus.calculatorplus.domain.Jogo[ idjogo=5 ]", Is.is(jogoTest.toString()));
        
    }
    
    @Test
    public void equalsTest(){
        Usuario usuarioTest = new Usuario();
        Jogo jogoTest = new Jogo();
        
        assertFalse(jogoTest.equals(usuarioTest));
        
    }
}
