/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aluno
 */
public class UsuarioTest {
    
    @Test
    public void loginValidaTrueTest(){
        Usuario user = new Usuario();
        user.setLogin("Loginteste");
        assertTrue(user.loginValida());            
    }
    
    @Test
    public void loginValidaFalseTest(){
        Usuario user = new Usuario();
        user.setLogin("Log1");
        assertFalse(user.loginValida());
    }
    @Test
    public void loginValidaFalseComEspacoTest(){
        Usuario user = new Usuario();
        user.setLogin("Login Teste");
        assertFalse(user.loginValida());
    }
    
    @Test
    public void senhaValidaTrueTest(){
        Usuario user = new Usuario();
        user.setSenha("senhaTeste01!@");
        assertTrue(user.senhaValida());
    }
    @Test
    public void senhaValidaFalseTest(){
        Usuario user = new Usuario();
        user.setSenha("senha");
        assertFalse(user.senhaValida());
    }
}
