/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import org.hamcrest.core.Is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dvk
 */
public class EOperatorTest {

    @Test
    public void calcularDivisaoPrimeiroMenorTest() {
        //Primeiro valor menor que o segundo
        double resultado = EOperator.divisao.calcular(5, 10);
        assertThat(resultado, Is.is(0.5));
    }

    @Test
    public void calcularDivisaoSegundoMenorTest() {
        //Segundo valor menor que o primeiro
        double resultado = EOperator.divisao.calcular(10, 5);
        assertThat(resultado, Is.is(2.0));
    }

    @Test
    public void calcularDivisaoNumerosIguaisTest() {
        //Números iguais
        double resultado = EOperator.divisao.calcular(10, 10);
        assertThat(resultado, Is.is(1.0));
    }

    @Test
    public void calcularMultiplicacaoTest() {
        //Números iguais
        double resultado = EOperator.multiplicacao.calcular(10, 10);
        assertThat(resultado, Is.is(100.0));
    }

    @Test
    public void calcularMultiplicacaoPorZeroTest() {
        //Multiplicar por zero
        double resultado = EOperator.multiplicacao.calcular(0, 10);
        assertThat(resultado, Is.is(0.0));

        resultado = EOperator.multiplicacao.calcular(10, 0);
        assertThat(resultado, Is.is(0.0));
    }

    @Test
    public void calcularSomaTest() {
        //Somar numeros positivos
        double resultado = EOperator.soma.calcular(10, 10);
        assertThat(resultado, Is.is(20.0));
    }
    
     @Test
    public void calcularSubtracaoTest() {
        //Subtrair numeros positivos
        double resultado = EOperator.subtracao.calcular(10, 10);
        assertThat(resultado, Is.is(0.0));
    }
}
