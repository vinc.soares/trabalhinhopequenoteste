/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action.dto;

import br.edu.vianna.plus.calculatorplus.util.Util;

/**
 *
 * @author daves
 */
public class RankingDTO {

    private String nome;
    private double bonus;
    private long tempo;
    private long numeroCompeticoes;

    public String getBonusFormatado() {
        return Util.formatarMoeda(bonus);
    }

    public RankingDTO() {
    }

    public RankingDTO(String nome, double bonus, long tempo, long numeroCompeticoes) {
        this.nome = nome;
        this.bonus = bonus;
        this.tempo = tempo;
        this.numeroCompeticoes = numeroCompeticoes;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public long getTempo() {
        return tempo;
    }

    public void setTempo(long tempo) {
        this.tempo = tempo;
    }

    public long getNumeroCompeticoes() {
        return numeroCompeticoes;
    }

    public void setNumeroCompeticoes(long numeroCompeticoes) {
        this.numeroCompeticoes = numeroCompeticoes;
    }
    
    

}
