/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ViewLoginAction implements IActionCommand {

    public ViewLoginAction() {
    }

    @Override
    public boolean ehSegura() {
        return false;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        if (request.getParameter("cpLogin") == null) {
            RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=login");
            rd.forward(request, response);
        } else {
            Usuario user = new UsuarioDao().findByLoginAndSenha(request.getParameter("cpLogin"), request.getParameter("cpSenha"));

            if (user == null) {
                request.setAttribute("msgErro", "Login ou Senha Incorreto" );
                RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=login");
                rd.forward(request, response);
            } else {
                request.getSession().setAttribute("user", user);
                new ViewDashboardAction().execute(request, response);
            }
        }

    }

}
