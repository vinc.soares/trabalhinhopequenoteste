/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author daves
 */
public class DaoGenerics {

    private static EntityManager instance;
    protected EntityManager conection;

    public DaoGenerics() {

        conection = DaoGenerics.getInstance();
    }

    public EntityManager getConection() {
        return conection;
    }

    public static synchronized EntityManager getInstance() {
        if (instance == null) {
            instance = Persistence.createEntityManagerFactory("PUcalculatorPlus").createEntityManager();
        }
        return instance;
    }
}
