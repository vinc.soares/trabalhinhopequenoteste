/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.time.LocalDate;
import javax.persistence.Query;

/**
 *
 * @author daves
 */
public class JogoDao extends DaoGenerics {

    public long getAllAcertos() {

        Query q = conection.createQuery("select count(j) from Jogo j where "
                + "j.resultado = j.resposta", Long.class);

        return (long) q.getSingleResult();

    }

    public long getAllErros() {

        Query q = conection.createQuery("select count(j) from Jogo j where "
                + "j.resultado <> j.resposta", Long.class);

        return (long) q.getSingleResult();

    }

    public long[] getAcertosPorDia() {
        Query q = conection.createQuery("select count(j) from Jogo j where "
                + "j.resultado = j.resposta and j.partida.data >= :paran and j.partida.data <= :paran1");
        long[] values = new long[10];
        for (int i = 1; i <= 10; i++) {
            q.setParameter("paran", Util.LocalDateTimeToDate(LocalDate.now().minusDays(10 - i).atStartOfDay()));
            q.setParameter("paran1", Util.LocalDateTimeToDate(LocalDate.now().minusDays(10 - i).atTime(23, 59, 59)));
            values[i - 1] = (long) q.getSingleResult();
        }
        return values;
    }

    public long[] getErrosPorDia() {
        Query q = conection.createQuery("select count(j) from Jogo j where "
                + "j.resultado <> j.resposta and j.partida.data >= :paran and j.partida.data <= :paran1");
        long[] values = new long[10];
        for (int i = 1; i <= 10; i++) {
            q.setParameter("paran", Util.LocalDateTimeToDate(LocalDate.now().minusDays(10 - i).atStartOfDay()));
            q.setParameter("paran1", Util.LocalDateTimeToDate(LocalDate.now().minusDays(10 - i).atTime(23, 59, 59)));
            values[i - 1] = (long) q.getSingleResult();
        }
        return values;
    }

    public long getAllAcertos(Integer id) {

        Query q = conection.createQuery("select count(j) from Jogo j where "
                + "j.resultado = j.resposta and j.partida.usuario.id = :id", Long.class);

        q.setParameter("id", id);
        return (long) q.getSingleResult();
    }

    public Partida saveRespostaUser(int id, double valor) {

        conection.getTransaction().begin();
        Jogo j = conection.find(Jogo.class, id);

        j.setResposta(valor);

        if (j.isCorrect()) {
            j.getPartida().addBonus(j.getBonus());
        } else {
            j.getPartida().removeBonus(j.getBonus() / 2);
        }
        conection.persist(j);
        conection.persist(j.getPartida());
        conection.getTransaction().commit();
        return j.getPartida();

    }

}
