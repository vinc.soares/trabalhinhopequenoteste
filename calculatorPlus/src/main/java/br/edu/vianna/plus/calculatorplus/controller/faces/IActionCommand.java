/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.faces;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aluno
 */
public interface IActionCommand {
    
    public boolean ehSegura();
    
    public void execute(HttpServletRequest request, 
                       HttpServletResponse response) throws Exception;
    
    
}
