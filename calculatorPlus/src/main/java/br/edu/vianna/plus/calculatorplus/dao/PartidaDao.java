/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import br.edu.vianna.plus.calculatorplus.util.Util;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author daves
 */
public class PartidaDao extends DaoGenerics {

    public double getAllBonus() {
        try {

            Query q = conection.createQuery("select sum(p.bonificacao) from Partida p", Double.class);

            return (double) q.getSingleResult();
        } catch (NoResultException e) {
            return 0;
        } catch (Exception e) {
            return 0;
        }

    }

    public List<Partida> getMinhasPartidas(Usuario user) {

        Query q = conection.createQuery("select p from Partida p where p.usuario.id = :id order by p.data desc");

        q.setParameter("id", user.getId());

        return q.getResultList();

    }

    public boolean userJaCompetiuHoje(Usuario user) {

        Query q = conection.createQuery("select count(p) from Partida p where "
                + "p.usuario.id = :id and p.data >= :paran and p.data <= :paran1");

        q.setParameter("id", user.getId());
        q.setParameter("paran", Util.LocalDateTimeToDate(LocalDate.now().atStartOfDay()));
        q.setParameter("paran1", Util.LocalDateTimeToDate(LocalDate.now().atTime(23, 59, 59)));

        return ((long) q.getSingleResult() >= 1);

    }

    public Partida iniciarPartida(Usuario user) throws SQLException {

        Partida part = new Partida(null, new Date(), 0,0);
        part.setUsuario(user);
        try {
            conection.getTransaction().begin();
            conection.persist(part);
            part.setJogoList(Jogo.criarJogosAleatorio(part, 10));
            for (Jogo jogo : part.getJogoList()) {
                jogo.setPartida(part);
                conection.persist(jogo);
            }
            conection.merge(part);
            conection.getTransaction().commit();
            return part;
        } catch (Exception e) {
            conection.getTransaction().rollback();
            throw new SQLException("Erro ao criar a partida :: " + e.getMessage());
        }

    }

    public Partida getPartidaIniciada(Usuario user) {

        Query q = conection.createQuery("select p from Partida p where "
                + "p.usuario.id = :id and p.data >= :paran and p.data <= :paran1", Partida.class);

        q.setParameter("id", user.getId());
        q.setParameter("paran", Util.LocalDateTimeToDate(LocalDate.now().atStartOfDay()));
        q.setParameter("paran1", Util.LocalDateTimeToDate(LocalDate.now().atTime(23, 59, 59)));
        final List<Partida> resultList = q.getResultList();

        return (resultList.isEmpty()) ? null : resultList.get(0);
    }

    public Partida save(Partida p) {
        conection.getTransaction().begin();
        conection.merge(p);
        conection.getTransaction().commit();
        return p;

    }

    public Partida getPartida(int idPartida) {
    
        return conection.find(Partida.class, idPartida);
    
    }

}
