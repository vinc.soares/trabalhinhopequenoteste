/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ViewRankingAction implements IActionCommand {

    public ViewRankingAction() {
    }

    @Override
    public boolean ehSegura() {
        return true;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        
            request.setAttribute("ranking", new UsuarioDao().getRanking());
            RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=ranking");
            rd.forward(request, response);

    }

}
