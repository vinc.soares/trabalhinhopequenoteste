<div class="col-md-6">

    <div class="card card-user">
        <div class="image stretch">
            <img src="./assets/img/bkg-1.png" alt="...">
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    <img class="avatar border-gray" src="./assets/img/relativity.png" alt="...">
                    <h3 class="title">Pergunta ${requestScope.perg}</h3>
                </a>
                <p class="description">
                    Resolva a quest�o a seguir:
                </p>
            </div>
            <p class="description text-center">
            <h1  class="text-center">${requestScope.jogo.valor1} ${requestScope.jogo.operador.operador} ${requestScope.jogo.valor2}</h1>
            </p>
        </div>
        <div class="card-footer">
            <hr>
            <div class="button-container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-6 ml-auto">
                        <h5>${requestScope.perg} / ${requestScope.total}
                            <br>
                            <small>Perguntas</small>
                        </h5>
                    </div>
                    <div class="col-lg-4 col-md-8 col-4 ml-auto mr-auto">
                        <form action="?ac=competicao" method="POST">
                            <input type="hidden" name="op" value="jog"/>
                            <input type="hidden" name="idjogo" value="${requestScope.jogo.idjogo}"/>
                            <h5><input type="text" class="form-control-danger"  name="cpResposta" required="true" placeholder="0" 
                                       autocomplete="off" autofocus
                                       value="">
                                <br>
                                <button type="submit" class="btn btn-primary btn-round">Pr�xima</button>
                            </h5>
                        </form>
                    </div>
                    <div class="col-lg-3 mr-auto">
                        <h5>Cr$ ${requestScope.bonus}
                            <br>
                            <small>b�nus</small>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
