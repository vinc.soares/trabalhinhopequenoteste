<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"> 
                <i class="nc-icon nc-trophy"></i>
                Ranking Geral</h4>
            <p class="category">Competições entre Alunos</p>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <tr><th>
                                Nome
                            </th>
                            <th>
                                Bonificação
                            </th>
                            <th>
                                Tempo de Jogo
                            </th>
                            <th >
                                Competições
                            </th>
                            <th class="text-right">
                                ...
                            </th>
                        </tr></thead>
                    <tbody>
                        <c:forEach items="${requestScope.ranking}" var="c" >
                            <tr>
                                <td>
                                    ${ c.nome}
                                </td>
                                <td>
                                    Cr$ ${c.bonusFormatado}
                                </td>
                                <td>
                                    ${c.tempo} minutos
                                </td>
                                <td>
                                    ${c.numeroCompeticoes}
                                </td>
                                <td class="text-right">
                                   ...
                                </td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
